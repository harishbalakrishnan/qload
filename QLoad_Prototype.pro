#-------------------------------------------------
#
# Project created by QtCreator 2014-11-07T01:39:56
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QLoad_Prototype
TEMPLATE = app
RC_FILE = myapp.rc

SOURCES += main.cpp\
        mainwindow.cpp \
    mpss_load.cpp

HEADERS  += mainwindow.h \
    mpss_load.h

FORMS    += mainwindow.ui \
    mpss_load.ui

RESOURCES += \
    MyResources.qrc


