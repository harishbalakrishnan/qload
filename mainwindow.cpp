#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "mpss_load.h"

#include <QtCore>
#include <QtGui>
#include <QMessageBox>
#include <QDir>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionAbout_triggered()
{
    QMessageBox::information(this,"About","QLoad v1.0     \n Developed by Harish");
}

void MainWindow::on_pushButton_clicked()
{

    QString mpss_path= ui->lineEdit->text();
    QDir valid_dir(mpss_path);
    if (valid_dir.exists())
    {
        MPSS_Load load_diaglog(this,mpss_path);
        load_diaglog.setModal(true);
        load_diaglog.exec();
    }
    else
    {
        QMessageBox::information(this,"Error","Invalid Path!");
    }

}
