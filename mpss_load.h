#ifndef MPSS_LOAD_H
#define MPSS_LOAD_H

#include <QDialog>
#include <QtCore>
#include <QtGui>

namespace Ui {
class MPSS_Load;
}

class MPSS_Load : public QDialog
{
    Q_OBJECT

public:
    explicit MPSS_Load(QWidget *parent = 0, QString Path="");
    ~MPSS_Load();

private slots:
    void on_pushButton_clicked();

private:
    Ui::MPSS_Load *ui;
    QString Full_Path,Path1;
    QStringListModel *available_flavours;
    QStringList flavours;

};

#endif // MPSS_LOAD_H
