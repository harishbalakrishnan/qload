#include "mpss_load.h"
#include "ui_mpss_load.h"
#include <QtCore>
#include <QtGui>
#include <QMessageBox>
#include <QDir>
#include <QProcess>



MPSS_Load::MPSS_Load(QWidget *parent,QString Path) :
    QDialog(parent),
    ui(new Ui::MPSS_Load)
{
    ui->setupUi(this);

    Path=Path+"\\modem_proc\\build\\ms\\bin\\";
    Path1=Path+"8952.gen.test\\";

    //Deciding to take Path or Path1
    QString Procedure1="";

    int i=0,j=0,k=0,flag=1;
    for(i=0;i<Path.length();i++)
    {
        if( flag==1 && Path[i]=='M' && Path[i+1]=='P' && Path[i+2]=='S' && Path[i+3]=='S')
        {
            j=i+5;
            while(Path[j]!='.')
            {
                Procedure1[k]=Path[j];
                k++;
                j++;
            }
            flag=0;
        }
    }

    if((Procedure1 =="JO")||(Procedure1 =="TA"))
    {
        Path=Path1;
    }
    this->Full_Path=Path;
    ui->label_4->setText(Full_Path);
    available_flavours= new QStringListModel(this);
    QDir MPSS_Dir(Full_Path);
    flavours=MPSS_Dir.entryList(QDir::Dirs);
    flavours.removeFirst();
    flavours.removeFirst();
    available_flavours->setStringList(flavours);
    ui->listView->setModel(available_flavours);
}

MPSS_Load::~MPSS_Load()
{
    delete ui;
}

void MPSS_Load::on_pushButton_clicked()
{

    QModelIndex index=ui->listView->currentIndex();
    QString selected_flavour= flavours.value(index.row());
    Full_Path=Full_Path+selected_flavour;

    //Finding the Procedure

    QString Procedure="";

    int i=0,j=0,k=0,flag=1;
    for(i=0;i<Full_Path.length();i++)
    {
        if( flag==1 && Full_Path[i]=='M' && Full_Path[i+1]=='P' && Full_Path[i+2]=='S' && Full_Path[i+3]=='S')
        {
            j=i+5;
            while(Full_Path[j]!='.')
            {
                Procedure[k]=Full_Path[j];
                k++;
                j++;
            }
            flag=0;
        }
    }
    qDebug() << Procedure;

    if(Procedure=="DPM")
        Procedure="c:/8916/Load.bat";
    else if(Procedure=="DI")
        Procedure="c:/Dime_Engg_Build_Load/Load.bat";
    else if(Procedure=="JO")
        Procedure="c:/8916/Load.bat";
    else if(Procedure=="BO")
        Procedure="c:/8916/Load.bat";
    else if(Procedure =="TA")
        Procedure="c:/8952/Load.bat";


    //Calling the Respective batch file

//    QString Bat_Arguement=Procedure+Full_Path;
//    qDebug() <<Bat_Arguement;
//    QProcess p;
//    p.start("cmd.exe", QStringList() << "/c" <<Bat_Arguement);
//    if (p.waitForStarted())
//    {
//       QMessageBox::information(this,"MPSS Loading","MPSS Loading Started.\nPlease wait for it to complete!");
//       p.waitForFinished(100000);
//       qDebug() << p.readAllStandardOutput();
//    }
//    else
//       qDebug() << "Failed to start";

       QProcess myProcess;
       myProcess.start(Procedure, QStringList() <<Full_Path);
       myProcess.waitForFinished(100000);
       qDebug() << myProcess.readAllStandardOutput();
       QMessageBox::information(this,"Alert","MPSS Loaded");
}

